<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    '/cidades' => 'CidadeController',
    '/estados' => 'EstadoController'
]);

//Route::resource('/cidades', 'CidadeController');
//Route::resource('/estados', 'EstadoController');


Route::resource('/estadosDB', 'EstadoDBController');
Route::get('/estados/pr/cidades', 'EstadoController@teste');

Route::get('/json', function () {
    return "Ola Json API";
});

Route::get('/cep', function () {

    $dados = array(
        "cep" => "85660000",
        "cidade" => "Dois Vizinhos",
        "uf" => "PR"
    );

    return $dados;

});



Route::get('/funcionarios', function () {
    $funcionarios = [
        array(
            "nome" => "Joao da silva",
            "idade" => 12
        ),
        array(
            "nome" => "Maria de Oliveira",
            "idade" => 20
        )
    ];

    $dados = array(
        "funcionarios" => $funcionarios,
        "qtd" => count($funcionarios)
    );

    return response()->json( $dados );

});


Route::post('/testepost',function (){ return response()->json(["msg"=>'Request Post']); } );
Route::delete('/testedelete',function (){ return response()->json(["msg"=>'Request Delete']); } );
Route::resource('/tester', 'EstadoController');



