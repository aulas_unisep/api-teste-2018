<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;
use Validator;
use App\Cidade;

class CidadeController extends Controller
{
    private $atributos = ['cidade', 'id_estado'];

    public function index()
    {
        //return Cidade::all();

        try{
            return response()->json( [Cidade::all()], 200 );
        }catch( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try{
//            $this->validate(
//                $request->all(),
//                [
//                    'cidade' => 'required|min:3|max:10',
//                    'id_estado' => 'required|numeric'
//                ]
//            );

//            echo "Teste";
//            exho "Teste";
//            exit;

            //Chamar validacao
            $validacao = $this->validar($request);

            $validacao->after(function ($validacao) {
                $validacao->errors()->add('campo1', 'Mensagem de validacao1!');
    //            $validacao->errors()->add('campo2', 'Mensagem de validacao2!');
    //            $validacao->errors()->add('campo3', 'Mensagem de validacao2!');
            });

            if ($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'Erro',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $cidade = new Cidade();

            $cidade->fill( $request->all() );
            $cidade->save();

            //Verifica se cadastrou a cidade no banco
            if( $cidade ){
                return response()->json( [$cidade], 201 );
            }else{
                return response()->json( ["mensagem" => "Erro ao cadastrar cidade"], 400 );
            }

            return $cidade;
        }catch ( \Exception $e ){
//            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{

            if( $id > 0 ){
                $cidade = Cidade::with('estado')->find( $id );
                if( $cidade ){
                    return $cidade;
                }else{
                    return response()->json( ["mensagem" => "Registro nao encontrado"], 404 );
                }
            }else{
                return response()->json( ["mensagem" => "Parametro invalido"], 400 );
            }


        }catch ( \Exception $e ){
            return response()->json( ["mensagem" => $e->getMessage()], 500 );
        }
//        $cidade = Cidade::find( $id );



//        return $cidade->estado;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $a = 0 / 0;
        }catch ( \Exception $e ){
            return $e->getMessage();;
        }
    }

    //Validacao dos campos
    public function validar( $request ){

        $validator = Validator::make($request->only( $this->atributos ),[
            'cidade' => 'required|min:3|max:10',
            'id_estado' => 'required|numeric'

        ]);



        return $validator;
    }
}
