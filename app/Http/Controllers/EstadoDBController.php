<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EstadoDBController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $estados = DB::select('select * from estados');

        //return "Contoller DB, nao ira utilizar model";

        return $estados;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //DB:insert("insert into estados () values( ? , ? )", []);
        //DB::insert("insert into estados ( descricao, uf ) values ( 'Santa Cataria' , 'Sc' )");

        //DB::insert("insert into estados ( descricao, uf ) values ( ? , ? )", [ "Santa", "SC" ]);

        DB::insert("insert into estados ( descricao, uf ) values ( ? , ? )",
            [ $request->input('descricao'), $request->input('uf') ]);

        return "Inserido com sucesso";


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $estados = DB::select('select * from estados where id = ? ', [$id]);

        //return "Contoller DB, nao ira utilizar model";

        return $estados;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estados = DB::update('update estados set descricao = ?, uf = ? where id = ? ',
            [$request->input('descricao'), $request->input('uf'), $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete("delete from estados where id = ? ", []);
    }
}
